﻿using MvcProjesi.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

  
namespace MvcProjesi.Controllers
{
   
    public class UyelikController : Controller

    {
        int kullaniciId = 0;
        Uye uye = new Uye();

        public List<Makale> Makales { get; set; }


      


        [HttpGet]
        public ActionResult BlogEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult BlogEkle(Makale b, HttpPostedFileBase file)
        {
                kullaniciId = Convert.ToInt32(Request.Form["kullaniciId"]);     // formdan gelen kullanici Id yakalandi
                
            try
            {
                using (MvcProjesiContext db = new MvcProjesiContext())
                {
                      
                    var uye = (from i in db.Uyes where i.UyeId == kullaniciId select i).SingleOrDefault();

                    Makale _blog = new Makale();

                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;

                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }

                    }
                    
                    _blog.Baslik = b.Baslik;
                    _blog.Icerik = b.Icerik;
                    _blog.Tarih = DateTime.Now;
                    _blog.Uye = uye;
                   
                    Makales = new List<Makale>() { _blog };
                     
                    db.Makales.Add(_blog);

                    db.SaveChanges();

                    return RedirectToAction("Admin", "Uyelik");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }


        public ActionResult Admin()
        {


            return View();
        }

        public ActionResult Uyelistesi()
        {
            MvcProjesiContext db = new MvcProjesiContext();
            List<Uye> makaleListe = (from i in db.Uyes select i).ToList();
            return View(makaleListe);

        }

        [HttpGet]
        public ActionResult YeniUyelik()
        {
            return View();
        }



        //Formumuzun geliş metodu Post
        //Dikkat ederseniz aynı isimli, iki adet Action var.
        //Üsttekinin metodu boş olduğu için Get oluyor.
        //Alttakinin üzerinde [HttpPost] olduğu için metodu Post oluyor.
        //Burada eğer sayfa içinden bir form gönderimi yapılmışsa, Post olan Action çağrılır.
        //Normal adres üzerinden sayfaya talepte bulunulursa, Get metodlu olan çağrılır.

        [HttpPost]
        public ActionResult YeniUyelik(Uye model, string textBoxDogum, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)        // Eger sınırlandirmalardan geçmis ise
            {
                return View();
            }
            if (String.IsNullOrEmpty(textBoxDogum))
            {
                //Burada Uye modelimizde olmayan bir elemanla çalıştığımız için, kendimiz elle hata

                //mesajını, sayfadaki hata listesine (@Html.ValidationSummary()) ekliyoruz.

                ModelState.AddModelError("textBoxDogum", "Doğum tarihi boş geçilemez");

                //Hata oluşması halinde sayfayı tekrar yüklüyoruz.

                //Böylelikle otomatik olarak hatalar sayfada gösteriliyor.
                return View();
            }
            int yil = int.Parse(textBoxDogum.Substring(6));
            if (yil > 2002)
            {
                ModelState.AddModelError("textBoxDogum", "Yaşınız 12 den küçük olamaz");
                return View();
            }



            if (file != null)
            {
                //Sunucuya dosya kaydedilirken, sunucunun dosya sistemini, yolunu bilemeyeceğimiz için
                //Server.MapPath() ile sitemizin ana dizinine gelmiş oluruz. Devamında da sitemizdeki
                //yolu tanımlarız.
                file.SaveAs(Server.MapPath("~/Images/") + file.FileName);
                uye.ResimYol = "/Images/" + file.FileName;
            }
            uye.Ad = model.Ad;
            uye.EPosta = model.EPosta;
            uye.Soyad = model.Soyad;
            uye.UyeOlmaTarih = DateTime.Now;
            uye.WebSite = model.WebSite;
            uye.Sifre = model.Sifre;
            using (MvcProjesiContext db = new MvcProjesiContext())
            {
                db.Uyes.Add(uye);
                db.SaveChanges();

                //İşlemimiz başarıyla biterse, başarılı olduğuna dair bir sayfaya yönlendiriyoruz.
                return RedirectToAction("UyelikBasarili");          // bu actionu dondurur
            }
        }

        public ActionResult UyelikBasarili()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UyeGiris()
        {
            return View();
        }

        [HttpPost]
        public string UyeGirisi()
        {
            //Request.Form["html elementinin name özelliği"] ile Post edilen formdaki elemanların

            //değerlerini alabiliyoruz. Bu metod yalnızca Post ile çalışır.

            string posta = Request.Form["txtPosta"];

            string sifre = Request.Form["pswSifre"];


            if (String.IsNullOrEmpty(posta) && String.IsNullOrEmpty(sifre))
            {

                return "E-Posta adresinizi ve şifrenizi girmediniz.";
            }
            else if (String.IsNullOrEmpty(posta))
            {
                return "E-posta adresinizi girmediniz.";
            }
            else if (string.IsNullOrEmpty(sifre))
            {
                return "Şifrenizi girmediniz.";
            }
            else
            {
                using (MvcProjesiContext db = new MvcProjesiContext())
                {
                    //Normalde şifreyi hashleyerek yazdırmamız ve kontrol etmemiz gerekir.

                    var uye = (from i in db.Uyes where i.Sifre == sifre && i.EPosta == posta select i).SingleOrDefault();

                    

                    if (uye == null) return "Kullanıcı adınızı ya da şifreyi hatalı girdiniz.";

                    //Session'da müşteri ile ilgili bilgileri saklamaktayız bu bilgileri istedigimiz viewlerde kullanabiliriz. giris yapan kullanicinin bilgilerini gosterme vb islemlerde.
                    //Güvenlik açısından bilgileri şifreleyerek saklamamız daha doğru bir yöntemdir.
                    //Asp.Net Membership yapısı, bu güvenliği sunmaktadır.
                    Session["uyeId"] = uye.UyeId;
                    Session["uyeAd"] = uye.Ad;
                    Session["uyeSoyad"] = uye.Soyad;
                    Session["uyeEposta"] = uye.EPosta;
                    Session["uyeResim"] = uye.ResimYol;
                    Session["uyeID"] = uye.UyeId;
                    return "Sisteme, başarıyla giriş yaptınız.<script type='text/javascript'>setTimeout(function(){window.location='Admin/'},3000);</script>";
                    //Burada eğer, kullanıcı bilgileri, sistemde eşleşirse, geriye girişin başarılı
                    //olduğuna dair bir mesaj ve 3 saniye sonra, ana sayfaya yönlendirecek bir
                    //javascript kodu ekliyoruz.
                }
            }



        }
    }
}