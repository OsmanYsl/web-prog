﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MvcProjesi.Controllers
{
    public class AdminLanguageController : Controller
    {
        // GET: AdminLanguage
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Change(String languageSelect)
        {
            if (languageSelect != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageSelect);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageSelect);
            }

            HttpCookie cookiee = new HttpCookie("language");
            cookiee.Value = languageSelect;
            Response.Cookies.Add(cookiee);
            
            return RedirectToAction("Index");
        }
    }
}