﻿//Veritabanı context sınıfımızı referans veriyoruz
using MvcProjesi.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MvcProjesi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        //Uygulama ilk başlatıldığında, buradaki metod çalışacak.
        protected void Application_Start()
        {
            //Burada veritabanı sınıfımızdan, bir nesne oluşturuyoruz. using kullanmamızın sebebi,
            //db nesnesinin işi bittiğinde, silinmesini ve hafızada yer tutmamasını sağlamak.
            using (MvcProjesiContext db = new MvcProjesiContext())
            {
                //Bu metod, eğer veritabanımız oluşturulmamış ise, oluşturulmasını sağlıyor.
                db.Database.CreateIfNotExists();

                //Veritabanındaki makalelerin, yorumların, üyelerin ve etiketlerin adetini alıyoruz.
                int makaleAdet = (from i in db.Makales select i).Count();
                int yorumAdet = (from i in db.Yorums select i).Count();
                int uyeAdet = (from i in db.Uyes select i).Count();
                int etiketAdet = (from i in db.Etikets select i).Count();

                //Veritabanına, sürekli aynı makalelerin ve yorumların eklenmemesi için
                //en az 5 adet makale ve yorum var mı diye kontrol ediyoruz.
                //Ayrıca sistemde en az 1 üye olduğunu da onaylıyoruz.
                //Bununla birlikte en az 3 adet etiket olduğunu da onaylıyoruz.
                if (uyeAdet<1 )
                {
                    //Bir tane örnek üye oluşturuyoruz.
                    Uye uye = new Uye() { Ad = "Osman", Soyad = "Yasal", EPosta = "osman_yasal@hotmail.com", ResimYol = null, UyeOlmaTarih = DateTime.Now, Sifre = "06031996" };

                    db.Uyes.Add(uye);

                    //Makalelerimizi oluşturuyoruz. Ayrıca makalelerin, yukarıda oluşturduğumuz kullanıcı 

                    //tarafından oluşturulduğunu gösteriyoruz.
                 
                    Makale makale1 = new Makale() {
                        Baslik = "Mvc nedir ?",
                        Tarih = DateTime.Now,
                        Uye = uye,
                        Icerik = "Mvc teknolojisi, model View ve controllerdan temel yapılarından oluşan 1970 lerde ortaya çıkan bir programlama yaklaşımıdır"
                        +" bu programlama yaklaşımı birbirini tekrar eden sürekli aynı şeylerden oluşan , yani model(veri tabanı) view(arayüz) controller(gidiş geliş ara işlemler)"
                        +" yapıların tekrarını görmüş ve bunları düzenlemiş bir yaklaşımdır. microsoft ise bunu .net platformuna geçirmiştir."
                          
                          };


                    Makale makale2 = new Makale()
                    { Baslik = "En basitinden c# nedir ?",
                        Tarih = DateTime.Now,
                        Uye = uye,
                        Icerik = "c# dili web application yapma yanında windows app. yapma ve başka platformalarda uygulama geliştirmek için microsoft tarafından ortaya çıkarışmış bir dildir"
                        +" Sakarya universitesinde genellikle 1. sınıfın 2. döneminde anlatılan bu dilin en büyük özelliklerinden birisi her platformda çalışması ve nesneye yönelimli olmasıdır."
                        ,
                    };
                    Makale makale3 = new Makale()
                    { Baslik = "c++ nedir ?",
                        Tarih = DateTime.Now,
                        Uye = uye,
                        Icerik = "c++ dili sakarya üniversitesinde  1.sınıfta öğretilen en temel ve öğrenmesi kolay dillerden 1 tanesidir."
                        +" nesneye yonelimli programlama imkanı yanında pointerlar sayesinde bellek üzerinde bize cambazlık yaptıran bir dildir. c dilinin geliştirilmiş halidir."
                    };
                    Makale makale4 = new Makale()
                    {
                        Baslik = "Merhabalar",
                        Tarih = DateTime.Now,
                        Uye = uye,
                        Icerik = "Siteme hoş geldiniz :) bu site genelde ders ağarlıklı olup , derslerle ilgili blogların paylaşıldığı plot bir sitedir"
                       + "Kayıt olan herkes bu paylaşımda bulunabilir. O halde hadi size kaydınızı oluşuturn :)"
                    };

                    //Makaleleri eklemek için komutumuzu veriyoruz.
                    //SaveChanges() komutu gelene kadar veritabanına kayıt yapılmayacak.
                    db.Makales.Add(makale1);
                    db.Makales.Add(makale2);
                    db.Makales.Add(makale3);
                    db.Makales.Add(makale4);

                    //Yorumlarımızı oluşturuyoruz. Ayrıca yorumların, yukarıda oluşturduğumuz kullanıcı 
                    //tarafından oluşturulduğunu gösteriyor, ayrıca makalelerimize de bağlıyoruz.
                    Yorum yorum1 = new Yorum() { Icerik = "Mvc teknolojisi bir harika !", Tarih = DateTime.Now, Makale = makale1, Uye = uye };
                    Yorum yorum2 = new Yorum() { Icerik = "Artık internet sitelerini kendi başıma yapabiliyorum teşekkürler mvc !", Tarih = DateTime.Now, Makale = makale1, Uye = uye };
                    Yorum yorum3 = new Yorum() { Icerik = "c# dilinin web programlamada kullanılması harika teşekkürler microsoft :)", Tarih = DateTime.Now, Makale = makale1, Uye = uye };
                    Yorum yorum4 = new Yorum() { Icerik = "c++ da proje geliştirmek çok güzel", Tarih = DateTime.Now, Makale = makale3, Uye = uye };
                    Yorum yorum5 = new Yorum() { Icerik = "c++ çok hızlı bir programlama dilidir", Tarih = DateTime.Now, Makale = makale3, Uye = uye };
                    Yorum yorum6 = new Yorum() { Icerik = "c++ ile hergün farklı farklı programlar yapabiliyorum anlaması çok kolay", Tarih = DateTime.Now, Makale = makale3, Uye = uye };

                    //Yorumları eklemek için komutumuzu veriyoruz.
                    //SaveChanges() komutu gelene kadar veritabanına kayıt yapılmayacak.
                    db.Yorums.Add(yorum1);
                    db.Yorums.Add(yorum2);
                    db.Yorums.Add(yorum3);
                    db.Yorums.Add(yorum4);
                    db.Yorums.Add(yorum5);
                    db.Yorums.Add(yorum6);

                    //Etiketlerimizi oluşturuyoruz. Ayrıca etiketleri, kullanıldığı makalelerimize de bağlıyoruz.
                    Etiket etiket1 = new Etiket() { Icerik = "Asp.Net", Makales = new List<Makale>() { makale1, makale2} };
                    Etiket etiket2 = new Etiket() { Icerik = "c++", Makales = new List<Makale>() { makale3 } };
                    Etiket etiket3 = new Etiket() { Icerik = "C#", Makales = new List<Makale>() { makale2,makale1 } };
                     
                    

                    //Etiketleri eklemek için komutumuzu veriyoruz.
                    //SaveChanges() komutu gelene kadar veritabanına kayıt yapılmayacak.
                    db.Etikets.Add(etiket1);
                    db.Etikets.Add(etiket2);
                    db.Etikets.Add(etiket3);
                     

                    //Son olarak da yaptığımız eklemelerin, veritabanına yansıtılmasını
                    //sağlamak için kaydet komutu veriyoruz.
                    db.SaveChanges();

                }
            }
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Language"];
            if (cookie != null && cookie.Value != null)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cookie.Value);
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cookie.Value);
            }
            else
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("tr");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("tr");
            }
        }
    }
}
